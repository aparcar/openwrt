name: Build staging snapshot

on:
  workflow_dispatch:
    inputs:
      targets:
        description: "Targets to build (x86/64 ath79/genric)"
        required: false
        default: "x86/64 ath79/generic"
      config:
        description: "Extra lines to append to the config"
        required: false
        default: ""

jobs:
  determine_targets:
    name: Set targets
    runs-on: ubuntu-latest
    outputs:
      targets: ${{ steps.find_targets.outputs.targets }}

    steps:
      - name: Checkout
        uses: actions/checkout@v2

      - name: Set targets
        id: find_targets
        run: |
          if [ "${{ github.event.inputs.targets }}" = "all" ]; then
            export TARGETS="$(perl ./scripts/dump-target-info.pl targets 2>/dev/null | awk '{ print $1 }')"
          else
            export TARGETS="${{ github.event.inputs.targets }}"
          fi

          JSON='{ "targets":['
          FIRST=1
          for TARGET in $TARGETS; do
            [[ $FIRST -ne 1 ]] && JSON="$JSON"','
            JSON="$JSON"'"'"${TARGET}"'"'
            FIRST=0
          done
          JSON="$JSON"']}'

          echo -e "\n---- targets ----\n"
          echo "$JSON"
          echo -e "\n---- targets ----\n"

          echo "::set-output name=targets::$JSON"

  build:
    name: Build ${{ matrix.targets }}
    needs: determine_targets
    runs-on: ubuntu-latest
    strategy:
      fail-fast: False
      matrix: ${{fromJson(needs.determine_targets.outputs.targets)}}

    steps:
      - name: Checkout
        uses: actions/checkout@v2
        with:
          fetch-depth: 0

      - name: Cache sources
        uses: davidsbond/cache@master
        with:
          path: dl/
          key: Sources
          update: True

      - name: Initialization environment
        env:
          DEBIAN_FRONTEND: noninteractive
        run: |
          sudo apt-get -y install libncurses-dev qemu-utils
          TARGET=$(echo ${{ matrix.targets }} | cut -d "/" -f 1)
          SUBTARGET=$(echo ${{ matrix.targets }} | cut -d "/" -f 2)
          echo "TARGET=$TARGET" >> "$GITHUB_ENV"
          echo "SUBTARGET=$SUBTARGET" >> "$GITHUB_ENV"

      - name: Update & Install feeds
        run: |
          ./scripts/feeds update -a
          ./scripts/feeds install -a

      - name: Set configuration
        run: |
          curl "https://downloads.openwrt.org/snapshots/targets/${{ matrix.targets }}/config.buildinfo" > .config
          for CONFIG in ${{ github.event.inputs.config }}; do
            echo "CONFIG_$CONFIG" >> .config
          done

          echo -e "\n---- config input ----\n"
          cat .config
          echo -e "\n---- config input ----\n"

          make defconfig

          echo -e "\n---- config post-defconfig ----\n"
          cat .config
          echo -e "\n---- config post-defconfig ----\n"

      - name: Download package
        run: |
          make download -j$(nproc)

      - name: Build tools
        run: |
          make tools/install -j$(nproc) || \
            make tools/install V=s

      - name: Build toolchain
        run: |
          make toolchain/install -j$(nproc) || \
            make toolchain/install V=s

      - name: Build target
        run: |
          make target/compile -j$(nproc) IGNORE_ERRORS='n m' || \
            make target/compile IGNORE_ERRORS='n m' V=s

      - name: Build packages
        run: |
          make package/compile -j$(nproc) IGNORE_ERRORS='n m' || \
            make package/compile IGNORE_ERRORS='n m' V=s

          make package/install -j$(nproc) || \
            make package/install V=s

          make package/index CONFIG_SIGNED_PACKAGES= V=s

      - name: Add kmods feed
        run: |
          TOPDIR=$(pwd)
          export TOPDIR
          STAGE_ROOT="$(make --no-print-directory val.STAGING_DIR_ROOT)"
          KERNEL_VERSION="$(make --no-print-directory -C target/linux \
              val.LINUX_VERSION val.LINUX_RELEASE val.LINUX_VERMAGIC | \
              tr '\n' '-' | head -c -1)"

          mkdir -p files/etc/opkg/
          sed -e 's#^\(src/gz .*\)_core \(.*\)/packages$#&\n\1_kmods \2/kmods/'"${KERNEL_VERSION}#" \
            "${STAGE_ROOT}/etc/opkg/distfeeds.conf" > files/etc/opkg/distfeeds.conf

          echo -e "\n---- distfeeds.conf ----\n"
          cat files/etc/opkg/distfeeds.conf
          echo -e "\n---- distfeeds.conf ----\n"

      - name: Build firmware
        run: |
          make target/install -j$(nproc) || \
            make target/install V=s

      - name: Buildinfo
        run: |
          make buildinfo V=s

      - name: JSON overview
        run: |
          make json_overview_image_info V=s

      - name: Checksum
        run: |
          make checksum V=s

      - name: Sanitize target
        run: echo "target_sani=$(echo ${{ matrix.targets }} | tr '/' '-')" >> "$GITHUB_ENV"
  
      - name: Upload images
        uses: actions/upload-artifact@v2
        with:
          name: ${{ env.target_sani }}-images
          path: bin/targets/${{ matrix.targets }}/openwrt-${{ env.TARGET }}-*
  
      - name: Upload packages
        uses: actions/upload-artifact@v2
        with:
          name: ${{ env.target_sani }}-packages
          path: |
            bin/targets/${{ matrix.targets }}/packages/*.ipk
            !bin/targets/${{ matrix.targets }}/packages/kmod-*.ipk
      - name: Upload kmods
        uses: actions/upload-artifact@v2
        with:
          name: ${{ env.target_sani }}-kmods
          path: bin/targets/${{ matrix.targets }}/packages/kmod-*.ipk
  
      - name: Upload supplementary
        uses: actions/upload-artifact@v2
        with:
          name: ${{ env.target_sani }}-supplementary
          path: |
            bin/targets/${{ matrix.targets }}/*.buildinfo
            bin/targets/${{ matrix.targets }}/*.json
            bin/targets/${{ matrix.targets }}/*.manifest
            bin/targets/${{ matrix.targets }}/kernel-debug.tar.zst
            bin/targets/${{ matrix.targets }}/openwrt-imagebuilder*
            bin/targets/${{ matrix.targets }}/openwrt-sdk*
            bin/targets/${{ matrix.targets }}/sha256sums*
      - name: Upload logs
        uses: actions/upload-artifact@v2
        with:
          name: ${{ env.target_sani }}-logs
          path: logs/
  
      - name: Prepare upload of kmods
        run: |
          mkdir -p bin/targets/${{ matrix.targets }}/kmods/${{ env.kernel_version }}/
          cp bin/targets/${{ matrix.targets }}/packages/kmod-*.ipk \
              bin/targets/${{ matrix.targets }}/kmods/${{ env.kernel_version }}/
